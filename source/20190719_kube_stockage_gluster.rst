Kubernetes & Storage GlusterFS
##############################


SRC : 
- https://github.com/gluster/gluster-kubernetes?utm_sq=g3kdatmu2w
- https://www.morot.fr/un-systeme-de-fichiers-haute-disponibilite-avec-glusterfs-paru-dans-glmf-144/

[Centos] : https://www.linuxtechi.com/setup-glusterfs-storage-on-centos-7-rhel-7/

Type: distribué, répliqué

Requis
======

- assez agnostic des machines ; 
- au moins deux machines ;
- machine avec un nom (DNS, hosts), n'utilise pas les ip pour les identifier
- SGF : supportant les attributs étendues (ext4 et XFS, celui recommandé)
- convention de nommage, non obligatoire **/data/glusterfs/volume/brick**
- LVM, non obligatoire pour le fonctionnement mais c'est une bonne pratique
- gluster supporte nativement NFS mais il n'est pas activé par défaut

Definition
==========

*Different Terminology used in GlusterFS storage :*
- **Trusted Storage Pool** : It is a group of multiple servers that trust each other and form a storage cluster.
- **Node** : A node is storage server which participate in trusted storage pool
- **Brick** : A brick is LVM based XFS (512 byte inodes) file system mounted on folder or directory.
- **Volume** : A Volume is a file system which is presented or shared to the clients over the network. A volume can be mounted using glusterfs, nfs and smbs methods.

*Different types of Volumes that can be configure using GlusterFS :*
- **Distribute Volumes** :It is the default volume which is created when no option is specified while creating volume. In this type of volume files will be distributed across the the bricks using elastic hash algorithm
- **Replicate Volumes** : As the name suggests in this type of volume files will replicated or mirrored across the bricks , In other words a file which is written in one brick will also be replicated to another bricks.
- **Striped Volumes** : In this type of volume larger files are cut or split into chunks and then distributed across the bricks.
- **Distribute Replicate Volumes** : As the name suggest in type of volume files will be first distributed among the bricks and then will be replicated to different bricks.

Commandes utilise
=================

Fichier:

*log*: /var/log/glusterfs/glustershd.log

.. code:: console

    # Contact VM du trusted pool
    gluster peer probe node1

    # Etat du cluster
    gluster peer status
    gluster volume status

    # creation volume sur les 2 nodes dans un dossier /data/glusterfs/vol0/brick0/
    gluster volume create repl-vol replica 2 transport tcp stor0:/data/glusterfs/vol0/brick0/ stor1:/data/glusterfs/vol0/brick0/

    gluster volume start repl-vol
    gluster volume info
    gluster volume status
    gluster volume list

    # CLient : montage d'un volume gluster
    mount -t glusterfs stor1:/repl-vol /data

    # Diminution temps basculement en cas d'arrêt d'un des nodes, passage des 42 sec par défaut à 5
    gluster volume set repl-vol network.ping-timeout 5

**NB** : même si le montage utilise le nom d'un node, il doit continuer à fonctionner si le node est down

Pour du test:

.. code:: console

    # Create de fichier aléatoire
    dd if=/dev/urandom of=/data/toto bs=1024 count=10240

Sur les machines client : install gluster-client. 

Sécurité
--------

Ajout d'une ACL pour limiter accès au volume, même principe que pour NFS

Deux options :
- auth.allow: \* (toutes)
- auth.reject: NONE

.. code:: console

    # Sur node : liste les machines pouvant y accèder
    gluster volume set repl-vol auth.allow 192.168.69.10
    gluster volume set repl-vol auth.allow 192.168.69.*
    
    gluster volume set repl-vol auth.allow vm1,vm2,master

Corbeille
---------

Espace pour conserver les fichiers supprimés. Peut être utilisé pour des opérations internes.

Création sur un volume précis ici repl-vol, taille 10Mo :

.. code:: console

    gluster volume set repl-vol features.trash on
    gluster volume set repl-vol features.trash-dir "Corbeille"
    gluster volume set repl-vol features.trash-max-filesize 10485760
    gluster volume set repl-vol features.trash-internal-op on


Troubleshooting
===============

Node HS 
-------

Passage du cluster en mode dégradé. Mais le service doit être assuré si le cluster est bien conf.

Migration d'un volume sur un autre node.

Arrêt manuel d'un node stor0

.. code:: console

    # Check état, perte contact avec stor0
    gluster volume heal repl-vol info

Reconstruction
~~~~~~~~~~~~~~

.. code:: console

    # 1/ provisionner un nouveau serveur
    gluster peer probe stor2

    # 2/ remplacement de la brick du stor0
    gluster volume replace-brick repl-vol stor0:/data/glusterfs/vol0/brick0 stor2:/data/glusterfs/vol0/brick0

    # 3/ réconciliation des volumes
    gluster volume heal repl-vol full

    # 4/ Synchronisation du volume
    gluster volume sync stor1 repl-vol

    # Il nous reste une dernière étape : répliquer le volume id dans les attributs étendus du système de fichiers et le propager au second serveur.

    # 5.1 récupération du volume id
    getfattr  -n trusted.glusterfs.volume-id

    # 5.2 propragation de l'id
    setfattr -n trusted.glusterfs.volume-id -v '0seEhN1zXZTFOXmRGV92ibvw==' /data/glusterfs/vol0/brick0/

    # Check
    gluster volume info repl-vol

    gluster peer detach stor0

    gluster pool list

?? les montages avec stor0 sont toujours foncitonnels

Extension volume
----------------

Contraintes : ajout un nombre de brick multiple du nombre de réplicas.

Si deux replicas, ajout de 2 ou 4 bricks.

.. code:: console

    gluster volume add-brick repl-vol stor3:/data/glusterfs/vol0/brick0 stor4:/data/glusterfs/vol0/brick0

    gluster volume info repl-vol

    # Les nouvelles bricks sont vides

    gluster volume rebalance repl-vol start
    gluster volume rebalance repl-vol status

Quotas par dossier
------------------

.. code:: console

    # Activation
    gluster volume quota repl-vol enable


    # quota sur un volume
    gluster volume quota repl-vol limit-usage / 100GB

    # quotas sur un sous-dossier
    # Création du dossier *subdir* depuis le client
    gluster volume quota repl-vol limit-usage /subdir 1GB

    # Check
    gluster volume quota repl-vol list


Install Heketi
==============

SRC: https://access.redhat.com/documentation/en-us/red_hat_gluster_storage/3.1/html/administration_guide/ch06s02

Context: cluster kube avec des nodes labellisée GlusterFS

Install heketi : pas d'obligation, de préférence sur le master-kube ou sa machine locale. La machine doit pouvoir joindre les nodes gluster.



.. code:: console

    VERSION=9.0.0 
    cd /tmp
    wget https://github.com/heketi/heketi/releases/download/$VERSION/heketi-$VERSION.linux.amd64.tar.gz
    tar xzvf heketi-$VERSION.linux.amd64.tar.gz
    mv heketi/heketi* /usr/local/bin/
    heketi -version
    # Heketi v$VERSION

Ouvrir une connexion ssh entre le node-heketi et tous les nodes-cluster GlusterFS.

- node-gluster : créer les dossiers :
    + /data/heketi/db
    + /data/heketi/.ssh
- node-heketi : générer les clés id_rsa
- node-gluster :
    + copie la paire de clés id_rsa dans /data/heketi/.ssh, tous les nodes ont la même
    + ajout id_rsa.pub comme authorized_key

