Kubernetes & Monitoring
#######################

Prometheus & Grafana
#######################

TUTO : 
Prometheus : https://linuxacademy.com/blog/kubernetes/running-prometheus-on-kubernetes/
Grafana : k8s-meo
AlertManager : @TODO

Version avec autres éléments

- AlertManager & Kube-state-metrics
https://medium.com/faun/production-grade-kubernetes-monitoring-using-prometheus-78144b835b60
https://github.com/Thakurvaibhav/k8s/tree/master/monitoring

- déploy avec Kustomize
https://kubernetes.github.io/ingress-nginx/user-guide/monitoring/

DOC: https://github.com/kubernetes/kubernetes/tree/master/cluster/addons/prometheus


Accès :

Pour accéder à l'interface de Prometheus
Commencez par ouvrir un tunnel SSH sur le port 9090 vers votre master :

.. code:: bash

    ssh -L 9090:127.0.0.1:9090 dada@IPDuMaster

    kubectl port-forward -n monitoring prometheus-prometheus-operator-prometheus-0 9090

Pour accéder à l'interface de Grafana
Encore un tunnel SSH, sur le 3000 ce coup-ci :

.. code:: bash

    ssh -L 3000:127.0.0.1:3000 dada@IPDuMaster

    kubectl port-forward $(kubectl get pods --selector=app=grafana -n monitoring --output=jsonpath="{.items..metadata.name}") -n monitoring 3000
