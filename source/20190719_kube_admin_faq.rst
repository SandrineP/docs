Comment faire une mise à jour propre de kube-controller-manager
---------------------------------------------------------------

1/ c'est un pod:
- modifier le manifest présent sur le/les masters '/etc/kubernetes/manifests/'
- redémarrer un des services kubelet sur un des masters


Comment supprimer un pod/ns en status terminating 
-------------------------------------------------

.. code:: console

    kubectl delete po --all --grace-period 0 --force

Si ça ne passe pas. 

- changer la date de suppression
- changer la liste des finalizers

.. code:: console

    apiVersion: v1
    kind: Pod
    metadata:
      annotations:
      creationTimestamp: "2019-07-16T15:45:38Z"
      deletionGracePeriodSeconds: 1
      deletionTimestamp: null
      finalizers: []

.. code:: console

    apiVersion: v1
    kind: Namespace
    metadata:
        [..]
    spec:
      finalizers: []

Soit les ressources sont supprimés (après quelques minutes ?)
Soit il faut relancer la commande de suppression



