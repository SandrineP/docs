Details resources
#################


https://blog.octo.com/extensions-kubernetes/


kind : Namespaces
=================

Deux états possibles:
- active
-terminating

Name : *Note that the name of your namespace must be a DNS compatible label.* ??

Finalizer : use pour la destruction des objets

Créer de plusieurs contextes, 

.. code:: console

    kubectl config set-context dev \
        --namespace=development --cluster=lithe-cocoa-92103_kubernetes --user=lithe-cocoa-92103_kubernetes

    kubectl config set-context prod \
        --namespace=production --cluster=lithe-cocoa-92103_kubernetes --user=lithe-cocoa-92103_kubernetes

    kubectl config use-context prod

PLUS: how you can provide different authorization rules for each namespace.

Moyen propre:
- ressources ;
- policies ;
- constraints (limit range, quota ...)
Scope (user community):
- ressources nommés
- délégation de l'autorité de gestion à un user de confiance ;
- possibilité de limiter utilisation des ressources par la communauté ;



kind: Pods
==========

Les custom finalizers permettent d’intercepter les destructions d’objets, en vue d’exécuter du code personnalisé. La terminologie fait référence aux destructeurs (finalizers) en programmation orientée objet. Le principe : Lorsqu’un objet (quelque soit son type) est créé, il est possible de déclarer une liste de finalizers.

hostname : 
    **10–1–1–1.default.pod.cluster.local**
    **IP.namespace.pod.cluster.local**



kind: Service
=============

Entrée DNS pour un service:

.. code:: console

    <service-name>.<namespace-name>.svc.cluster.local


kind: Statefullset
==================

- utiliser pour le déploiement statefull : pour des base de données ou autres solutions qui gérent des données en local


kind: Secret
============

Deux possibilités pour y accèder depuis un container :
- en variable d'environnement lisible par tous ;
- en mount sur un dossier tmpfs : plus adapter pour la sécurité :

.. code:: yaml

    
    containers:
        [..]
        volumeMounts:
        - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
          name: default-token-79kkc
          readOnly: true

      volumes:
      - name: default-token-79kkc
        secret:
          defaultMode: 420
          secretName: default-token-79kkc


kind: Namespaces
================

En prod, il faut prendre en compte avec la gestion des namespaces de :
- network policies ;
- restriction sur les quotas.


Label
=====

Syntax strict :
- peut comporter un prefix suivi d'un slash, il doit correspondre à un subdomain DNS valide
- longeur:
    + prefix: 253 max [a-zA-Z0-9.-_]
    + value :  63 max [a-zA-Z0-9.-_]

Composant master
================

API server
----------

3 APIs disponible :
- kubernetes API **/api/v1/<resources>**
    => list, get, create, update objects
- autoscaling API **apis/autoscaling/v1**
    => dédié au scale horizontal automatique, basé sur le CPU, ou autres métriques
- batch API **/apis/batch/v1**
    => gestion des jobs, list, query, create, delete

etcd
----

Controller manager
------------------

Composant du master
Collection de controllers divers qui assure la cohérence entre la demande et l'existent
    => replication controller, pod controller, service controller, endpoint controller

Sheduler
---------

Schedule les pods sur les nodes en fonction de plusieurs contraintes:
- pre-requis en resources
- pre-requis en service
- contrainte au niveau hard/soft-ware
- spécification sur affinité/antiaffinité
- localisation des données
- date limites ??


DNS
---

- depuis v1.3, pod 
- utiliser par tous les services, sauf le headless pour obtenir un nom DNS
- pod recoivent aussi un nom DNS
- très utile pour la découverte automatique


Composant node
==============

proxy
-----

- forwarding TCP/UDP 
- gére les services au niveau du node 
- collecte des infos IPs, dns name via les variables environnements ajoutées dans les containers


kubelet
-------

- gére les containers sur les nodes
- actions:
    + télécharge les secrets des pods depuis API 
    + montage des volumes
    + lancement des containers 
    + report le status des pods et du node
    + exécute les liveness probe

?? readness probe : par qui ?

Interface
=========

Utilisation des interfaces à plusieurs niveaux pour être agnostiques des technologies :
- CRI runtime interface
- CSI
- CNI 

Runtime container
==================

docker
------

Changement depuis la version 1.11 (avril 2016):
- use OCI
- use containerd and runC
=> docker-engine -> containerd -> runc ou autre runtime compatible OCI