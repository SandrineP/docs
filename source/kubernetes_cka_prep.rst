CKA - Kube : préparation


SRC @llogiou 
    https://github.com/Krast76/libvirt
    https://paste.krast.net/ewuhuteduj.pl
    https://github.com/dgkanatsios/CKAD-exercises
    https://docs.google.com/document/d/1AMVwvVabPoYt-o1k8Uo7UlmlfsjQKVHDhDyKP3QqbOM/edit

Return:
    https://medium.com/@sovmirich/preparing-and-passing-the-certified-kubernetes-administrator-cka-exam-4a76fa4b1c4



Sujets
######



Recommandations
###############

- bien connaitre les commandes kubectl (toutes celles du cheatsheet)
- connaitre le jsonpath
- maitriser les ressources: pod, deploy, daemonset, service
- savoir consulter les logs : des applications et des services
- connaitre les pod static (sans controller)
- TLS bootstrapping


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Service annexes cluster

monitoring, 
logging, 
security, 
backup,
DR: Disaster Recovery strategy

Management cluster

**kyverno** : custom ressources pour gérer les policies 
- appliquer des contraintes sur les pull d'images ;
- surcharger des pod/deploy pour appliquer les mêmes règles partout : ? voir la partie commune avec PodPreset

SRC: https://www.nirmata.com/2019/07/11/managing-kubernetes-configuration-with-policies/