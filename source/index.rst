.. demo documentation master file, created by
   sphinx-quickstart on Wed Jul 24 16:51:42 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to demo's documentation!
================================

.. toctree::

   20190715_kube_doc
   20190718_kube_dashboard
   20190718_kube_install_monitoring
   20190719_kube_admin_faq
   20190719_kube_disaster_recovery
   20190719_kube_stockage_gluster
   20190719_kube_tps
   20190723_kube_detail_resources
   devops_k8s_complete_course
   index
   kube_articles
   kube_books
   kubernetes_cka_prep

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
