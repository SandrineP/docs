Kubernetes & Books
##################

Kubernetes: Up and Running
##########################

Date: septembre2017

Apport d'un cluster Kubernetes

- velocity (rapidité) : déploiement et mise à jour d'application à large échelle. Tous les composants sont liées entre eux ;
- immutabilité : les mises à jours apparaissent comme une accumulation de modification plutôt qu'un remplacement d'une version ancienne par une nouvelle ;
- declarative configuration pour la création des ressources kube : description d'un état et pas lancement d'action via des commandes. Offre une solution plus facile à debugger et peut intégrer dans les outils classique avec le code applicatif ;
- shelf-healing systems : API et controler pour installer l'état demandée et le maintenir 
- scaling your service and your teams : avec l'augmentation des besoins dans le cluster. Solution: une architecture découplée grâce à l'API et les LBs. 
    + API : buffer entre déploiement et utilisation : découpage d'une application en ressources, chaque service va pouvoir gérer la sienne ;
    + LBs : buffer entre les instances en cours d'exécution : permet de scaler facilement une application, le service découvre automatiquement la liste des pods à servir.
- easy scaling application and cluster : des ressources matérielles sont partagées entre les équipes, ce n'est des serveurs dédiés à une équipes/projets
- scaling development teams with microservices : bonne taille d'équipes est de 6/8 personnes car facile pour le partage des connaissances, rapidité de la prise de décision et sens commun des buts. Mais souvent les besoins pour le développement d'une application sont supérieure à cela, d'où l'avantage du découpage en microservice avec une petite équipe qui gére quelques microservices ;
- abstracting your infrastructure : utilise d'interface pour s'abstraire de la solution sous jacente permet d'être portable dans différents environnement mais implique d'utiliser des solutions open-sources.
- efficiency : 


Clean image on serveurs

https://github.com/spotify/docker-gc


Jobs
####

Gestion : si un job échoue, kube en recrée un nouveau

Also, due to the nature of distributed systems there is a small chance, during certain failure scenarios, that duplicate pods will be created for a specific task.

Job pattern : en fonction du nombre de pod dans le template la terminaison du job et la parallélisation se comporte différemment. [p24].

Le job, après un exécution réussi, est stoppé mais persiste sur le cluster le temps de consulter les logs (param du manifest ?)

kubectl get jobs       # all Running
kubectl get jobs -a    # all Running and stopped

Cas échec en boucle (crashLoopBack) et police de redémarrage :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- *restartPolicy: OnFailure* : le même pod restart en boucle 
- *restartPolicy: Never* : le pod n'est pas redémarré mais le job n'est pas réalisé donc il recrée un pod. Mauvaise stratégie : accumulation de pod en échec avant d'être supprimé et conso de ressources.


Cas parallélisation des tâches: 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On a besoin de 10 execution du containers et on veut parallèliser par 5. 

.. code:: block

    spec:
        parallelism: 5
        completions: 10
        template:
            metadata:
            labels:
                chapter: jobs
        spec:
            containers:
            - name: kuard

Cas work queues 
~~~~~~~~~~~~~~~~

Succession de tâches qui utilise le rendu de la prédécente



Mastering Kubernetes
####################

Copyright © 2017 Packt Publishing
Kube version demo 1.4.3


Faiblesses: 

- sécurité ;
- difficultés à déployer une application multicontainers, surtout niveau réseau
- dev, monitoring,  logging
- limitation des containers a ne lancer qu'une commande en entrypoint
- publier trop rapidement des fonctionnalités de base

Create cluster
===============

Minikube
--------

Opérations réalisées au lancement de minikube :

- Create a VirtualBOx VM
- Set up boot2docker
- Create certificates for the local machine and the VM
- Set up networking between the local machine and the VM
- Run the local Kubernetes cluster on the VM


kubeadm
-------

cloud provider
--------------

baremetal
---------

Questions préalables: 
- Implementing your own cloud-provider interface or sidestepping it
- Choosing a networking model and how to implement it (CNI plugin,
direct compile)
- Whether or not to use network policy
- Select images for system components
- Security model and SSL certificates
- Admin credentials
- Templates for components such as API Server, replication controller,
and scheduler
- Cluster services: DNS, logging, monitoring, and GUI


Administration
==============

Monitoring
----------

Logging 
-------

Elements complémentaires
-------------------------

Node problem detector
~~~~~~~~~~~~~~~~~~~~~

- daemonset solide et fiable qui permet d'identifier un problème sur un node non détecter par ailleurs :
    + CPU, memoire 
    + problème disque
    + kernel deadlock
    + SFG corrompu
    + dysfonctionnement du daemon docker
=> sert à remonter les infos les infos utiles sur l'hote, Cela peut représenter beaucoup d'information
- doit nécessaire peu de ressources pour ne pas impacter les autres pods

Faire une séparation entre la collecte des info par des daemonset et la résolution avec *remedy controller*.

Architecture robuste
--------------------

- identifier les cas d'échec et les conséquences et coûts associés
- besoin :
    + stratégie de gestion des incidents
    + procédures de recouvrement

Deux types d'échec au niveau hardware:

- node n'y est pas sensible : plus difficile à identifier:
    + problème de réseau
    + problème de configuration
    + échec du hardware
    => rien n'apparait dans les logs du node actuelle. Remonter plus loin dans le temps : rechercher des messages d'erreur ou des pertes de performances

- node y est sensible: subit les problèmes du hardware. On peut utiliser les données remontés par le node detector. Analyser le comportement des pods ou de jobs sur le node (lenteur, redémarrage)

Si le problème est isolé sur un seul node, tenté un redémarrage pour résoudre le problème.

Sur-provisionner son cluster pour avoir une marge en cas de perte d'un node (totale ou partiel).

Quotas, partage et limites
--------------------------

Comment le détecter et le résoudre ?

- ressources insuffisantes : 
    + manque de ressources au démarrage du pod : jamais lancé
- sous-utilisation:
    + réservation de trop de resources, non disponible pour d'autres pods
- configuration de node incompatible : 
    + ex: beaucoup de CPU et peu de Mem : pod va se placer sur un node et utilise tout son CPU, pas d'autres pods présents sur le node 


**NB** : bien prendre en compte les besoins d'un pod pour tout son cycle de vie, au démarrage il peut y avoir un pic (app java).


Mauvaise configuration
----------------------

Peut recouvrir beaucoup de choses

Pistes:
- labels érronnés : pod, node, container ?
- placement de pods sans replication controller
- erreur dans les ports des services
- erreur dans le configmap

Pour les résoudre :
- avoir une chaine de déploiement automatique 
- après tout changement ou déploiement bien vérifier l'état apps et cluster
- vérifier les paramètres de performances

Cost vs performances
--------------------

Avoir suivi les performances et les besoins sur le temps pour anticiper les augmentations à venir. Augmentation des connexions ou des resources d'une application (nouveau user, nouvelles fonctionnalités).

Dépend de l'environnement de déploiement du cluster :
- Cloud : très souple pour ajouter ou diminuer les nodes, mais avec un coût 
- Bare metal : attention à pouvoir étendre le cluster en fonction de besoin rapidement
- Hybrid : fusionner les résultats de chacun des clusters avec les caractéristiques propres.



